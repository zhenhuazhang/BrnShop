﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using TaskTest.Model;

namespace TaskTest
{
    public class ParallelDbTest
    {
        public static void TestMethod()
        {
            ParallelDbTest dbTest=new ParallelDbTest();
            //dbTest.InsertDataByEF();
            //dbTest.InsertDataBySqlBulkCopy();

            dbTest.ParallelSelect();
            //dbTest.Select();

            //dbTest.SaveLoadData();
            //dbTest.LoadData();

            //dbTest.Deomo1();
        }

        public void InsertDataByEF()
        {
            using (ParallelTestDbEntities context=new ParallelTestDbEntities())
            {
                Stopwatch sw=new Stopwatch();
                sw.Start();
                context.Configuration.AutoDetectChangesEnabled = false;
                for(int i = 0;i<1000000;i++ )
                {
                    ParallelTable table=new ParallelTable();

                    table.Age = i%100 + 1;
                    table.Col1 = (i + 1).ToString();
                    table.Col2 = (i + 2).ToString();
                    table.Col3 = (i + 3).ToString();
                    table.Col4 = (i + 4).ToString();
                    table.Col5 = (i + 5).ToString();
                    table.Col6 = (i + 6).ToString();
                    context.ParallelTable.Add(table);
                }
                //context.ParallelTable.
                context.SaveChanges();
                sw.Stop();

                Console.WriteLine("并行插入100W条数据所需要的时间:{0}",sw.ElapsedMilliseconds);
            }
        }

        public void InsertDataBySqlBulkCopy()
        {
            Stopwatch sw=new Stopwatch();
            sw.Start();
            using (ParallelTestDbEntities context=new ParallelTestDbEntities())
            {
                using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(context.Database.Connection.ConnectionString))
                {
                    DataTable dt=new DataTable();
                    dt.Columns.Add(new DataColumn("Age",typeof(Int32)));
                    dt.Columns.Add(new DataColumn("Col1"));
                    dt.Columns.Add(new DataColumn("Col2"));
                    dt.Columns.Add(new DataColumn("Col3"));
                    dt.Columns.Add(new DataColumn("Col4"));
                    dt.Columns.Add(new DataColumn("Col5"));
                    dt.Columns.Add(new DataColumn("Col6"));

                    for(int i=0;i< 10000000; i++)
                    {
                        var row = dt.NewRow();
                        row["Age"] = i%100 + 1;
                        row["Col1"] = i + 1;
                        row["Col2"] = i + 2;
                        row["Col3"] = i + 3;
                        row["Col4"] = i + 4;
                        row["Col5"] = i + 5;
                        row["Col6"] = i + 6;

                        dt.Rows.Add(row);
                    }
                    sqlBulkCopy.ColumnMappings.Add("Age", "Age");
                    sqlBulkCopy.ColumnMappings.Add("Col1", "Col1");
                    sqlBulkCopy.ColumnMappings.Add("Col2", "Col2");
                    sqlBulkCopy.ColumnMappings.Add("Col3", "Col3");
                    sqlBulkCopy.ColumnMappings.Add("Col4", "Col4");
                    sqlBulkCopy.ColumnMappings.Add("Col5", "Col5");
                    sqlBulkCopy.ColumnMappings.Add("Col6", "Col6");
                    sqlBulkCopy.DestinationTableName = "ParallelTable";
                    sqlBulkCopy.BulkCopyTimeout = 50000000;
                    sqlBulkCopy.WriteToServer(dt);
                }
            }
            sw.Stop();

            Console.WriteLine("SqlBulkCopy插入100W条数据所需要的时间:{0}", sw.ElapsedMilliseconds);
            
        }

        public void ParallelSelect()
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            using (ParallelTestDbEntities context =new ParallelTestDbEntities() )
            {
                var pt=context.ParallelTable.AsParallel().Where(w => w.Age <= 50&&w.Col1.Contains("12"));

                var cnt = pt.Count();

                Console.WriteLine("filter count:{0}",cnt);
            }
            sw.Stop();
            Console.WriteLine("ParallelSelect:{0}", sw.ElapsedMilliseconds);
            Console.WriteLine("==========================");
        }

        public void Select()
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            using (ParallelTestDbEntities context = new ParallelTestDbEntities())
            {
                var pt = context.ParallelTable.Where(w => w.Age <= 50);

                var cnt = pt.Count();

                Console.WriteLine("filter count:{0}", cnt);
            }
            sw.Stop();
            Console.WriteLine("Select:{0}", sw.ElapsedMilliseconds);
            Console.WriteLine("==========================");
        }

        public ConcurrentBag<Student> SaveLoadData()
        {
            ConcurrentBag<Student> students = new ConcurrentBag<Student>();
            Stopwatch sw=new Stopwatch();
            sw.Start();
            Parallel.For(0, 15000000, i =>
            {
                var stu = new Student();
                stu.Name = "nam" + i.ToString();
                stu.CreateTime = DateTime.Now;
                stu.Sex = i%2 != 0;
                stu.Age = i%100;

                students.Add(stu);
            });
            sw.Stop();
            Console.WriteLine("Save List:{0}", sw.ElapsedMilliseconds);
            Console.WriteLine("==========================");

            return students;
        }

        public List<Student> LoadData()
        {
            List<Student> students = new List<Student>();
            Stopwatch sw = new Stopwatch();
            sw.Start();
            for(int i=0; i<15000000;i ++)
            {
                var stu = new Student();
                stu.Name = "nam" + i.ToString();
                stu.CreateTime = DateTime.Now;
                stu.Sex = i % 2 != 0;
                stu.Age = i % 100;

                students.Add(stu);
            };
            sw.Stop();
            Console.WriteLine("List:{0}", sw.ElapsedMilliseconds);
            Console.WriteLine("==========================");

            return students;
        }

        public void Deomo1()
        {
            var pdata = this.SaveLoadData();
            Stopwatch sw=new Stopwatch();
            sw.Start();
            var pCnt = pdata.AsParallel().Count(w => w.Age > 50 && w.Sex);
            sw.Stop();
            Console.WriteLine("Parallel Linq:{0},Count{1}", sw.ElapsedMilliseconds,pCnt);
            Console.WriteLine("==========================");

            var data = this.LoadData();
            
            sw.Restart();
            var cnt = data.Count(w => w.Age > 50 && w.Sex);
            sw.Stop();
            Console.WriteLine("Normal Linq:{0},Count:{1}", sw.ElapsedMilliseconds,cnt);
            Console.WriteLine("==========================");
        }
    }

    public class Student
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public bool Sex { get; set; }
        public DateTime CreateTime { get; set; }
    }
}

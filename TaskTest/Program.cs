﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TaskTest
{
    class Program
    {
        static void Main(string[] args)
        {
            //GetName("name1").GetAwaiter().GetResult();
            //GetName("name2");
            //GetName("name3");//.GetAwaiter().GetResult();

            //GetNameAsync("Name1");
            //GetNameAsync("Name2");
            //GetNameAsync("Name3");
            
            //ParallelTest.Test();

            //ParallelDbTest.TestMethod();

            //测试数据库更新并发
            ConcurrencyForMySql.TestMothed();

            Console.WriteLine("this is main thread");
            Console.ReadLine();
        }

        private static Task GetName(string name)
        {
            return Task.Factory.StartNew(()=>Console.WriteLine("My name is {0}",name));
            Thread.Sleep(10);
        }

        private static async void GetNameAsync(string name)
        {
            Task task = Task.Run(() =>
            {
                Thread.Sleep(500);
                Console.WriteLine("My name is {0}", name);
            });
            await task;
        }
    }
}

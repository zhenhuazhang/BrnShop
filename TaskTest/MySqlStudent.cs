﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;

namespace TaskTest
{
    [Table("Student")]
    public class MySqlStudent:IRowVersion
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }

        public byte[] RowVersion { get; set; }
    }
    /// <summary>
    /// 控制并发，必须是byte类型
    /// </summary>
    public interface IRowVersion
    {
        byte[] RowVersion { get; set; }
    }

    public class MySqlTestContext : DbContext
    {
        public MySqlTestContext()
            :base("mysqlTest")
        { }

        public DbSet<MySqlStudent> Students { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Properties().Where(w=>typeof(IRowVersion).IsAssignableFrom(w.DeclaringType)&&
                w.PropertyType==typeof(byte[])&&w.Name=="RowVersion").
                Configure(c=>c.IsConcurrencyToken().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None));
            base.OnModelCreating(modelBuilder);
        }

        public override int SaveChanges()
        {
            this.ChangeTracker.DetectChanges();
            var objectContext = ((IObjectContextAdapter) this).ObjectContext;
            foreach (var stateEnt in objectContext.ObjectStateManager.GetObjectStateEntries(EntityState.Modified|EntityState.Added))
            {
                var row = stateEnt.Entity as IRowVersion;
                if (row!=null)
                {
                    row.RowVersion = System.Text.Encoding.UTF8.GetBytes(Guid.NewGuid().ToString());
                }
            }

            return base.SaveChanges();
        }
    }
}

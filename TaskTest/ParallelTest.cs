﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using TaskTest.Model;

namespace TaskTest
{
    /// <summary>
    /// 并行测试
    /// </summary>
    public class ParallelTest
    {
        private List<int> data=new List<int>(); 
        private List<int> stopData=new List<int>(); 
        public ParallelTest()
        {
            //System.Collections.Concurrent
            for (int i = 0; i < 10; i++)
            {
                data.Add(i+1);
            }
            for (int i = 0; i < 100000000; i++)
            {
                stopData.Add(i+1);
            }
        }

        private bool showProcessExecution = false;
        public void Demo1()
        {
            Stopwatch watch=new Stopwatch();
            watch.Start();
            int sum = 0;
            for (int i = 0; i < data.Count; i++)
            {
                Thread.Sleep(50);
                sum += data[i];
                Console.WriteLine(data[i]);
            }

            watch.Stop();
            Console.WriteLine("普通For运行时长：{0},总和{1}",watch.ElapsedMilliseconds,sum);
        }

        public static void Test()
        {
            ParallelTest pt = new ParallelTest();
            pt.Demo7();
            pt.Demo8();
        }

        public void Demo2()
        {
            Stopwatch watch = new Stopwatch();
            watch.Start();
            int sum = 0;
            foreach (var i in data)
            {
                Thread.Sleep(50);
                sum += i;
                Console.WriteLine(i);
            }

            watch.Stop();
            Console.WriteLine("普通Foreach运行时长：{0},总和{1}", watch.ElapsedMilliseconds,sum);
        }

        public void Demo3()
        {
            Stopwatch watch = new Stopwatch();
            watch.Start();
            int sum = 0;
            Parallel.For(0,data.Count, (i) =>
            {
                Thread.Sleep(50);
                sum += data[i];
                Console.WriteLine(data[i]);
            });

            watch.Stop();
            Console.WriteLine("并行For运行时长：{0},总和{1}", watch.ElapsedMilliseconds,sum);
        }

        public void Demo4()
        {
            Stopwatch watch = new Stopwatch();
            watch.Start();
            int sum = 0;
            Parallel.ForEach(data, (d) =>
            {
                Thread.Sleep(50);
                sum += d;
                Console.WriteLine(d);
            });

            watch.Stop();
            Console.WriteLine("并行ForEach运行时长：{0},总和{1}", watch.ElapsedMilliseconds,sum);
        }

        public void Demo5()
        {
            Stopwatch watch = new Stopwatch();
            watch.Start();
            //int sum = 0;
            Parallel.ForEach(data, (d,loopState) =>
            {
                Thread.Sleep(50);
                if(d>10)
                    loopState.Break();
                //sum += d;
                Console.WriteLine(d);
            });

            watch.Stop();
            Console.WriteLine("并行ForEach中断运行时长：{0}", watch.ElapsedMilliseconds);
        }

        public void Demo6()
        {
            Stopwatch watch = new Stopwatch();
            watch.Start();
            //int sum = 0;
            Parallel.ForEach(data, (d, loopState) =>
            {
                Thread.Sleep(50);
                if (d > 10)
                    loopState.Stop();
                //sum += d;
                Console.WriteLine(d);
            });

            watch.Stop();
            Console.WriteLine("并行ForEach中断运行时长：{0}", watch.ElapsedMilliseconds);
        }

        public void Demo7()
        {
            Stopwatch watch = new Stopwatch();
            watch.Start();
            //int sum = 0;

            var tempData=stopData.AsParallel().Where(i => i < 90000&&i>88880);

            watch.Stop();

            foreach (var data in tempData)
            {
                //Console.WriteLine(data);
            }

            
            Console.WriteLine("并行AsParallel运行时长：{0}", watch.ElapsedMilliseconds);
        }

        public void Demo8()
        {
            Stopwatch watch = new Stopwatch();
            watch.Start();
            //int sum = 0;
            var tempData = stopData.Where(i => i < 90000 && i > 88880);
            watch.Stop();

            foreach (var data in tempData)
            {
                //Console.WriteLine(data);
            }

            
            Console.WriteLine("普通Where运行时长：{0}", watch.ElapsedMilliseconds);
        }

        public void Demo9()
        {
            //using (BrnShopEntities context=new BrnShopEntities())
            //{
            //    context.bsp_adminactions.AsParallel()
            //}
        }
    }
}
